﻿using System.ComponentModel.DataAnnotations;

namespace HardCodeTestProject.Models;

public class ProductCategory
{
    [Key] public int Id { get; set; }
    [Required] public string Name { get; set; }

    public List<ProductAttribute> Attributes { get; set; } = new();
}