﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HardCodeTestProject.Models;

public class ProductAttributeValue
{
    [Key] public int Id { get; set; }
    [Required] public string Value { get; set; }

    [ForeignKey(nameof(ProductAttribute))] public int AttributeId { get; set; }
}