﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HardCodeTestProject.Models;

public class Product
{
    [Key] public int Id { get; set; }
    [Required] public string Name { get; set; }

    [ForeignKey(nameof(ProductCategory))] public int CategoryId { get; set; }
    public List<ProductAttributeValue> AttributeValues { get; set; } = new();
}