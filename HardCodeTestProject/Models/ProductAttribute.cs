﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HardCodeTestProject.Models;

public class ProductAttribute
{
    [Key]
    public int Id { get; set; }
    [Required]
    public string Name { get; set; }
    
    [ForeignKey(nameof(ProductCategory))]
    public int CategoryId { get; set; }
}