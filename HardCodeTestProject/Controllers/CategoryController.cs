﻿using HardCodeTestProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HardCodeTestProject.Controllers;

[ApiController]
[Route("[controller]")]
public class CategoryController : ControllerBase
{
    private readonly ProductContext _db;

    public CategoryController(ProductContext db)
    {
        _db = db;
    }

    public class CategoryAndAttributes
    {
        public string CategoryName { get; set; }
        public List<string> AttributeNames { get; set; }
    }

    [HttpGet]
    public ActionResult<List<ProductCategory>> Get()
    {
        return _db.Categories.AsNoTracking()
            .Include(c => c.Attributes)
            .AsNoTracking()
            .ToList();
    }

    [HttpGet("{id}")]
    public ActionResult<ProductCategory> Get(int id)
    {
        var category = _db.Categories.AsNoTracking()
            .Include(c => c.Attributes)
            .AsNoTracking()
            .FirstOrDefault(c => c.Id == id);

        if (category is null)
            return NotFound();

        return category;
    }

    [HttpPost]
    public IActionResult Post(CategoryAndAttributes categoryAndAttributes)
    {
        var category = new ProductCategory()
        {
            Name = categoryAndAttributes.CategoryName,
            Attributes = categoryAndAttributes.AttributeNames.ConvertAll(name => new ProductAttribute() { Name = name })
        };

        _db.Categories.Add(category);
        _db.SaveChanges();

        return CreatedAtAction(nameof(Get), new { id = category.Id }, category);
    }
}