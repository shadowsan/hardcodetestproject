﻿using HardCodeTestProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HardCodeTestProject.Controllers;

[ApiController]
[Route("[controller]")]
public class AttributeController : ControllerBase
{
    private readonly ProductContext _db;

    public AttributeController(ProductContext db)
    {
        _db = db;
    }

    [HttpGet]
    public ActionResult<List<ProductAttribute>> GetAttributes()
    {
        return _db.Attributes
            .AsNoTracking()
            .ToList();
    }

    [HttpGet("{categoryId}")]
    public ActionResult<List<ProductAttribute>> GetAttributesByCategory(int categoryId)
    {
        return _db.Attributes
            .AsNoTracking()
            .Where(attr => attr.CategoryId == categoryId)
            .ToList();
    }

    [HttpGet("Value/{id}")]
    public ActionResult<ProductAttributeValue> GetPav(int id)
    {
        var pav = _db.AttributeValues.FirstOrDefault(av => av.Id == id);

        if (pav is null)
            return NotFound();

        return pav;
    }

    [HttpGet("Value")]
    public ActionResult<List<ProductAttributeValue>> GetPavByAttributeId([FromQuery] int attrId)
    {
        return _db.AttributeValues
            .AsNoTracking()
            .Where(av => av.AttributeId == attrId)
            .ToList();
    }

    [HttpPost("Value")]
    public IActionResult Post(ProductAttributeValue pav)
    {
        var attribute = _db.Attributes.AsNoTracking().FirstOrDefault(attr => attr.Id == pav.AttributeId);
        if (attribute is null)
            return NotFound(new { pav.AttributeId });

        _db.AttributeValues.Add(pav);
        _db.SaveChanges();

        return CreatedAtAction(nameof(GetPav), new { id = pav.Id }, pav);
    }
}