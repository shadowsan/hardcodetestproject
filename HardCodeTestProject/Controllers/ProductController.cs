﻿using System.Linq.Expressions;
using HardCodeTestProject.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HardCodeTestProject.Controllers;

[ApiController]
[Route("[controller]")]
public class ProductController : ControllerBase
{
    private readonly ProductContext _db;

    public ProductController(ProductContext db)
    {
        _db = db;
    }

    public class ProductAndAttributeValues
    {
        public string Name { get; set; }
        public int CategoryId { get; set; }

        public List<int> AttributeValueIds { get; set; } = new();
    }

    public class FilterProductAttributeValues
    {
        public string? Name { get; set; }
        public int? CategoryId { get; set; }
        public List<int> AttributeValueIds { get; set; } = new();
    }

    [HttpGet]
    public ActionResult<List<Product>> Get([FromQuery] FilterProductAttributeValues filterPAV)
    {
        var attributeValues = _db.AttributeValues
            .AsNoTracking()
            .Where(av => filterPAV.AttributeValueIds.Contains(av.Id));

        return _db.Products
            .AsNoTracking()
            .Include(p => p.AttributeValues)
            .AsNoTracking()
            .Where(p => filterPAV.CategoryId == null || p.CategoryId == filterPAV.CategoryId)
            .Where(p => attributeValues.All(av => p.AttributeValues.Any(pav => pav.Id == av.Id)))
            .ToList();
    }

    [HttpGet("{id}")]
    public ActionResult<Product> Get(int id)
    {
        var product = _db.Products.AsNoTracking().FirstOrDefault(p => p.Id == id);

        if (product is null)
            return NotFound();

        return product;
    }

    [HttpPost]
    public IActionResult Post(ProductAndAttributeValues productAndAttributeValues)
    {
        // check if category exists
        var category = _db.Categories.AsNoTracking()
            .Include(c => c.Attributes)
            .AsNoTracking()
            .FirstOrDefault(c => c.Id == productAndAttributeValues.CategoryId);

        if (category is null)
            return NotFound(new { CategoryId = productAndAttributeValues.CategoryId });

        var attributeValues = _db.AttributeValues
            .Where(av => productAndAttributeValues.AttributeValueIds.Contains(av.Id)).ToList();

        // if al attributeValues exist
        if (attributeValues.Count != productAndAttributeValues.AttributeValueIds.Count)
            return NotFound(new { AttributeValues = productAndAttributeValues.AttributeValueIds });

        // if all attributeValues belong to the proper category
        if (!attributeValues.All(av => category.Attributes.Exists(attr => attr.Id == av.AttributeId)))
            return NotFound(new { AttributeValues = productAndAttributeValues.AttributeValueIds });

        var product = new Product
        {
            Name = productAndAttributeValues.Name,
            CategoryId = category.Id,
            AttributeValues = attributeValues
        };

        _db.Products.Add(product);
        _db.SaveChanges();

        return CreatedAtAction(nameof(Get), new { id = product.Id }, product);
    }
}