﻿using HardCodeTestProject.Models;
using Microsoft.EntityFrameworkCore;

namespace HardCodeTestProject;

public class ProductContext : DbContext
{
    private string DbPath { get; }

    public DbSet<ProductCategory> Categories { get; set; }
    public DbSet<ProductAttribute> Attributes { get; set; }
    public DbSet<Product> Products { get; set; }
    public DbSet<ProductAttributeValue> AttributeValues { get; set; }

    public ProductContext(DbContextOptions options) : base(options)
    {
        var folder = Environment.SpecialFolder.LocalApplicationData;
        var path = Environment.GetFolderPath(folder);
        DbPath = Path.Join(path, "product.db");
    }

    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        base.OnConfiguring(options);
        options.UseSqlite($"Data Source={DbPath}");
        options.EnableSensitiveDataLogging();
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        // link foreign key of attributes to category when creating category with attributes
        modelBuilder.Entity<ProductCategory>()
            .HasMany(e => e.Attributes)
            .WithOne()
            .HasForeignKey(e => e.CategoryId)
            .IsRequired();
    }
}